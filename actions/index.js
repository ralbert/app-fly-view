import axios from 'axios';
//import _ from 'lodash';

const APPID = 'ca16e6dc';
const APPKEY = 'a22c7096dd56e958b27ce9cf342c5cc7';
//const airportCode = 'ABQ';

const axiosInstance = axios.create({
    baseURL: 'https://api.flightstats.com/',
    timeout: 2000,
});

export const searchWeather = airportCode => { // no hacen falta los parentesis al rededor de singerName ya es es solo un atributo
    return axiosInstance.get(`flex/weather/rest/v1/json/all/${airportCode}?appId=${APPID}&appKey=${APPKEY}`).then (
        (response) => {
            const weather = response.data.zoneForecast
             return weather;
        })
        /*
    return axiosInstance.get(`${airportCode}?appId=${APPID}&appKey=${APPKEY}`).then (
       (data) => {
           debugger;
            return data;
       })*/
}
export const searchDataWeather = airportCode => { // no hacen falta los parentesis al rededor de singerName ya es es solo un atributo
    return axiosInstance.get(`flex/weather/rest/v1/json/all/${airportCode}?appId=${APPID}&appKey=${APPKEY}`).then (
        (response) => {
            const data = response.data.metar.tags
             return data;
        })
}
export const searchAirports = radious => { // no hacen falta los parentesis al rededor de singerName ya es es solo un atributo
    return axiosInstance.get(`flex/airports/rest/v1/json/withinRadius/-84/10/${radious}?appId=${APPID}&appKey=${APPKEY}`).then (
        (response) => {
            const airports = response.data.airports;
            return airports;
        })
}

export const searchDelays = country => { // no hacen falta los parentesis al rededor de singerName ya es es solo un atributo
    return axiosInstance.get(`flex/delayindex/rest/v1/json/country/${country}?appId=${APPID}&appKey=${APPKEY}&classification=5&score=3`).then (
        (response) => {
            const delays = response.data.delayIndexes;
            return delays;
        })
}
