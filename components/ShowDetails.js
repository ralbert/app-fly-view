import React from 'react';
import { View } from 'react-native';
import {Text} from 'react-native-elements';

export class ShowDetails extends React.Component {
  renderData() {
    const { data, titleKey} = this.props; //por que proviene de AlternativeAlbumScreen a travez de props
    return data.map((item, index) => {
        return (         
            <Text key={index}> {item.key}: {item.value} </Text>
        )
    })
  }

  render() {
    const { data } = this.props;  //destructuring

    if ( data && data.length > 0 ){
      return this.renderData();
    } else {
      return <View></View>
    }
    
  }
}