import React from 'react';
import { View } from 'react-native';
import { Card, Text, Image } from 'react-native-elements';
const URL = 'https://airportprofessional.asn.au/wp-content/uploads/2018/02/tap-adl.jpg'

export class DelayList extends React.Component {
  renderData() {
    const { data} = this.props; //por que proviene de AlternativeAlbumScreen a travez de props
    return data.map((item, index) => {
        return (
            <Card           
                key={index} //index es el numero de arriba y va a ser único en cada iteración
                title={item.airport.name}
                image={{uri: URL}}
                >  
             
                <Text>City: {item.airport.city}</Text> 
                <Text>State: {item.airport.stateCode}</Text>   
                <Text>Address: {item.airport.street1}</Text>
                <Text>15 min after: {item.delayed15}</Text> 
                <Text>30 min after: {item.delayed30}</Text>
                <Text>45 min after: {item.delayed45}</Text>           
            </Card>
        )
    })
  }

  render() {
    const { data } = this.props;  //destructuring
   
    if ( data.length > 0 ){
      return this.renderData();
    } else {
      return <View></View>
    }
    
  }
}