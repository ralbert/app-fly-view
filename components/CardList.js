import React from 'react';
import { View } from 'react-native';
import { Card, Text } from 'react-native-elements';

export class CardList extends React.Component {
  renderData() {
    const { data, titleKey, image} = this.props; //por que proviene de AlternativeAlbumScreen a travez de props
    return data.map((item, index) => {
        return (
            <Card           
                key={index} //index es el numero de arriba y va a ser único en cada iteración
                title={item.name}
                image={{uri: image}}
                >     
                <Text>City: {item.city}</Text>   
                <Text>Country: {item.countryName}</Text>   
                <Text>Region: {item.regionName}</Text>     
            </Card>
        )
    })
  }

  render() {
    const { data } = this.props;  //destructuring
   
    if ( data.length > 0 ){
      return this.renderData();
    } else {
      return <View></View>
    }
    
  }
}