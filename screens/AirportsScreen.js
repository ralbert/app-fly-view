import React from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { Card, Text, Button } from 'react-native-elements';
import { CardList } from '../components/CardList';
import { SearchText } from '../components/SearchText';

import * as actions from '../actions';

const URL = 'https://upload.wikimedia.org/wikipedia/commons/9/92/Washington_Dulles_International_Airport_at_Dusk.jpg'

export default class AirportsScreen extends React.Component {
    static navigationOptions = {
        title: 'Airports',
    };

    constructor() {
        super();

        this.state = {
            airports: [],  //si se coloca esto en null en el cardList habrá que crear una condicion para evitar que el map genere un error cuando se levata el componente
            isFeching: false // para colocar un valor por defecto antes de que se haaga una busqueda
        } 

        this.searchAirports = this.searchAirports.bind(this);  //duda con esto    
    }
    
    searchAirports(airport) {//esta puede ser enviada por Props como se muestra ne la linea 34
        this.setState({isFeching: true, airports: []});
        actions.searchAirports(airport)
        .then(airports => this.setState({airports, isFeching: false}))
        .catch(err => this.setState({airports: [], isFeching: false}));  
    }


    renderAirportView() { 
        const airports = this.state.airports;
        const isFeching = this.state.isFeching;

        return (
            <ScrollView style={styles.container}>   
                <SearchText submitSearch={this.searchAirports}></SearchText> 
                { airports.length > 0 && !isFeching &&
                    <CardList   data={airports} 
                                titleKey={'title'}
                                image = {URL}
                                >
                    </CardList>
                } 
                { airports.length === 0 && isFeching &&
                    <Text>I am on it...</Text>
                }


            </ScrollView>
        )     
    }

    render() {  
        return this.renderAirportView();
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});

