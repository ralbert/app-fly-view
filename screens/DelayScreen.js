import React from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { Card, Text, Button } from 'react-native-elements';
import { DelayList } from '../components/DelayList';
import { SearchText } from '../components/SearchText';

import * as actions from '../actions';

export default class DelayScreen extends React.Component {
    static navigationOptions = {
        title: 'Current Delays',
    };

    constructor() {
        super();

        this.state = {
            delays: [],  //si se coloca esto en null en el cardList habrá que crear una condicion para evitar que el map genere un error cuando se levata el componente
            isFeching: false // para colocar un valor por defecto antes de que se haaga una busqueda
        } 

        this.searchDelays = this.searchDelays.bind(this);  //duda con esto    
    }
    
    searchDelays(country) {//esta puede ser enviada por Props como se muestra ne la linea 34
        this.setState({isFeching: true, delays: []});
        actions.searchDelays(country)
        .then(delays => this.setState({delays, isFeching: false}))
        .catch(err => this.setState({delays: [], isFeching: false}));  
    }


    renderDelaysView() { 
        const delays = this.state.delays;
        const isFeching = this.state.isFeching;

        return (
            <ScrollView style={styles.container}>   
                <SearchText submitSearch={this.searchDelays}></SearchText> 
                { delays.length > 0 && !isFeching &&
                    <DelayList   data={delays} 
                                titleKey={'title'}
                                >
                    </DelayList>
                } 
                { delays.length === 0 && isFeching &&
                    <Text>I am on it...</Text>
                }


            </ScrollView>
        )     
    }

    render() {  
        return this.renderDelaysView();
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});