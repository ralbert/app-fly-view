import React from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { Card, Text, Button } from 'react-native-elements';

import * as actions from '../actions';
import {ShowDetails} from '../components/ShowDetails';
import { SearchText } from '../components/SearchText';

const URL = 'https://www.thesouthafrican.com/wp-content/uploads/2018/10/rain-weather-1140x570.jpg'


export default class WeatherScreen extends React.Component {
    static navigationOptions = {
        title: 'Weather',
    };

    constructor() {
        super();

        this.state = {
                weather: [],
                data: [],
                isFeching: false
        }    
        
        this.searchWeather = this.searchWeather.bind(this); // es opcional si se usa la linea 42
    }

    searchWeather(airport) { //method to send the value to make the request getting it from the input components
        this.setState({isFeching: true, weather: [], data: []});
        actions.searchWeather(airport)
        .then(
            (weather) => {
                this.setState({weather: weather, isFeching: false})
            }
        )
        .catch(err => this.setState({weather: [], isFeching: false}));

        actions.searchDataWeather(airport)
        .then(
            (data) => {
                this.setState({data: data, isFeching: false})
            }
        )
        .catch(err => this.setState({weather: [], isFeching: false}));

    }

    renderWeatherView(){
        //<SearchText submitSearch={(airport) => {this.searchWeather(airport)}}></SearchText>   de esta forma no requiero del bind
        const weather = this.state.weather;
        const detailsData = this.state.data;
        const isFeching = this.state.isFeching;
        
        return (
            <ScrollView style={styles.container}>      
                <SearchText submitSearch={this.searchWeather}></SearchText>   
                { detailsData.length > 0 && !isFeching &&
                    <Card
                        title={weather.header}
                        image={{uri: URL}}>
                        <ShowDetails
                            data={detailsData} 
                            titleKey={'title'}
                            >
                        </ShowDetails>
                    </Card> 
                } 
                { detailsData.length === 0 && isFeching &&
                    <Text>I am on it...</Text>
                }
                   
            </ScrollView>
        );
    }


    render() {
        return this.renderWeatherView();
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});